import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  content: any;
  constructor(private appService: AppService) { }

  ngOnInit() {
    this.getContent();
  }

  getContent() {
    this.appService.showLoading();
    this.appService.appPost({}, 'getAboutContent')
      .subscribe(response => {
        if (response.body.status) {
          this.content = response.body.data;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        }
      });
  }
}
