import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {
  user: any = {};
  userForm: FormGroup;
  changePsForm: FormGroup;
  isProfile = 'profile';
  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastController: ToastController
  ) {
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
    console.log(this.user)
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', { disabled: true }]
    });

    this.changePsForm = this.formBuilder.group({
      opassword: ['', Validators.required],
      password: ['', Validators.required],
      repassword: ['', Validators.required]
    }, { validator: this.passwordCompaire });
  }

  passwordCompaire(frm: FormGroup) {
    return frm.controls['password'].value === frm.controls['repassword'].value ? null : { 'mismatch': true };
  }
  editProfile() {
    this.isProfile = 'edit';
    this.userForm.setValue({
      name: this.user.name,
      email: this.user.email,
      phone: this.user.phone
    });
  }

  changePassword() {
    this.isProfile = 'changePs';
  }
  setProfile() {
    this.isProfile = 'profile';
  }

  imageUpload(event) {
    console.log(event.target.files[0])
    const data = new FormData();
    data.append('image', event.target.files[0]);
    data.append('Body', JSON.stringify(this.user));
    this.appService.postUrlEncoded(data, 'updateProfile')
      .subscribe(response => {
        if (response.status) {
          this.user.name = response.data.name;
          this.user.phone = response.data.phone;
          this.user.profile_image = response.data.image;
          this.user.email=response.data.email;
          localStorage.setItem('a_c_uid', JSON.stringify(this.user));
        }
      });
  }

  saveUser() {
    this.appService.showLoading();
    const form = new FormData();
    form.append('Body', JSON.stringify({ ...this.userForm.value, token: this.user.token }));
    this.appService.postUrlEncoded(form, 'updateProfile')
      .subscribe(response => {
        if (response.status) {
          this.user.name = response.data.name;
          this.user.phone = response.data.phone;
          this.user.profile_image = response.data.image;
          this.user.email=response.data.email;
          localStorage.setItem('a_c_uid', JSON.stringify(this.user));
          setTimeout(() => {
            this.appService.hideLoading();
          }, 1000);
          this.presentToast(response.msg);
        } else {
          setTimeout(() => {
            this.appService.hideLoading();
          }, 1000);
          this.presentToast(response.msg);
        }

      });
  }
  savePassword() {
    this.appService.showLoading();
    this.appService.appPost(this.changePsForm.value, 'changePassword')
      .subscribe(response => {
        if (response.body.status) {
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
          this.presentToast(response.body.msg);
          this.router.navigate(['/home']);
        } else {
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
          this.presentToast(response.body.msg);
        }
      });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      animated: true
    });
    toast.present();
  }
}
