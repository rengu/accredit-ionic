import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/_service/app.service';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { ModalController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from'@logisticinfotech/ionic4-datepicker';
import * as moment from 'moment';
@Component({
  selector: 'app-request-soa',
  templateUrl: './request-soa.component.html',
  styleUrls: ['./request-soa.component.scss'],
})
export class RequestSOAComponent implements OnInit {
  gPdf: any = {};
  loan;
  data: any;
  isRecord = false;
  datePickerObj:any;
  mindate:any;
  today1:any;
  constructor(
    private appService: AppService,
    private base64ToGallery: Base64ToGallery,
    private fileOpener: FileOpener,
    public modalController: ModalController,
    private file: File) { }

  ngOnInit() {
    this.mindate= new Date('1980-03-14');
    var today1 = new Date();
    this.today1=new Date();
    this.today1=moment().format('DD-MM-YYYY');
    this.gPdf.date_to=  this.today1;
    console.log(this.today1);
   
  }
  async openDatePickerto()
  {
    this.datePickerObj = {
      dateFormat: 'DD-MM-YYYY',
      showTodayButton:false,
     // fromDate: new Date('1980-03-14'), // default null
    //  fromDate:this.mindate,
     // toDate: this.mindate, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    };
      var item =item;
      var selectedDate=new  Date();
      const datePickerModal = await this.modalController.create({
       
        component: Ionic4DatepickerModalComponent,
        cssClass: 'li-ionic4-datePicker',
        componentProps: { 
           'objConfig': this.datePickerObj, 
           'selectedDate':selectedDate,
           'titleLabel': 'Select a Date',
           'dateFormat': 'DD-MM-YYYY'
        }
      });
      await datePickerModal.present();
      datePickerModal.onDidDismiss()
        .then((data) => {
          if(data.data.date != "Invalid date")
          {
            this.gPdf.date_to=data.data.date;
            console.log(data.data.date,'sdfsdfsf');
          }
          
        });
    
  }
  async openDatePickerfrom()
  {
    this.datePickerObj = {
      dateFormat: 'DD-MM-YYYY',
      showTodayButton:false,
     // fromDate: new Date('1980-03-14'), // default null
      fromDate:this.mindate,
     // toDate: this.mindate, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    };
      var item =item;
      var selectedDate=new  Date();
      const datePickerModal = await this.modalController.create({
        component: Ionic4DatepickerModalComponent,
        cssClass: 'li-ionic4-datePicker',
        componentProps: { 
           'objConfig': this.datePickerObj, 
           'selectedDate':selectedDate,
          
           'titleLabel': 'Select a Date',
           'dateFormat': 'DD-MM-YYYY'
        }
      });
      await datePickerModal.present();
      datePickerModal.onDidDismiss()
        .then((data) => {
          if(data.data.date != "Invalid date")
          {
            this.mindate=new Date(data.data.date);
            this.gPdf.date_from=data.data.date;
            console.log(data.data.date,'sdfsdfsf');
          }
          
        });
    
  }
  getPDF() {
    this.appService.showLoading();
    this.appService.appPost({ ...this.gPdf, ...this.loan }, 'downloadBulkPDF')
      .subscribe(response => {
        console.log(response)
        if (response.body.status) {
          this.isRecord = false;
          this.data = response.body.data;
          const downloadPDF: any = response.body.url;
          console.log(response.body.pdf_server_url);
          
    
         /*  fetch(downloadPDF, {
            method: 'GET'
          }).then(res => res.blob()).then(blob => {
            this.file.writeFile(this.file.externalRootDirectory, 'soa.pdf', blob, {
              replace: true
            }).then(res => {
              window.open(response.body.pdf_server_url, '_self');
               /*  this.fileOpener.open(
                res.toInternalURL(),
                'application/pdf'
              ).then((res1) => {
                console.log('res' + res1);
              }).catch(err => {
                console.log('open error')
              });   

            }).catch(err => {
              console.log('save error')
            });
            console.log(downloadPDF);
          }) */
          setTimeout(() => {
            //window.open(response.body.pdf_server_url, '_self', 'location=yes');
            window.open(response.body.pdf_server_url);
            this.appService.hideLoading();
          }, 500);
        } else {
          this.isRecord = true;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        }
      });
  }
}
