import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaymentHistoryPage } from './payment-history.page';
import { RepaymentHistoryDetailsPage } from './repayment-history-details/repayment-history-details.page';
import { RequestSOAComponent } from './request-soa/request-soa.component';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
const routes: Routes = [
  {
    path: '',
    component: PaymentHistoryPage
  },  {
    path: 'details',
    component: RepaymentHistoryDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ionic4DatepickerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PaymentHistoryPage,
    RepaymentHistoryDetailsPage,
    RequestSOAComponent
  ],
  entryComponents: [RequestSOAComponent],
  providers: [Base64ToGallery]
})
export class PaymentHistoryPageModule {}
