import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.page.html',
  styleUrls: ['./payment-history.page.scss'],
})
export class PaymentHistoryPage implements OnInit {
  scheduleList: any[] = [];
  scheduleList1: any[] = [];
  noData1: any;
  noData2: any;
  constructor(
    private appService: AppService
    ) { }

  ngOnInit() {
    this.getScheduleList();
  }

  getScheduleList() {
    this.appService.showLoading();
    this.appService.appPost({}, 'getRePaymentScheduleList')
      .subscribe(response => {
        if (response.body.status) {
          if(response.body.data.Inactive.length >0){
            this.scheduleList = response.body.data.Inactive;
          }else{
            this.noData1 = 'Record not found !'; 
          }
          if(response.body.data.Active.length >0){
             this.scheduleList1 = response.body.data.Active;
          }else{
            this.noData2 = 'Record not found !';
          }
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        } else {
          this.noData1 = response.body.msg;
          this.noData2 = response.body.msg;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        }
      });
  }


}
