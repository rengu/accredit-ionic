import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/_service/app.service';
import { ModalController } from '@ionic/angular';
import { RequestSOAComponent } from '../request-soa/request-soa.component';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import {InAppBrowser, InAppBrowserOptions,InAppBrowserEvent} from '@ionic-native/in-app-browser/ngx';
@Component({
  selector: 'app-repayment-history-details',
  templateUrl: './repayment-history-details.page.html',
  styleUrls: ['./repayment-history-details.page.scss'],
})
export class RepaymentHistoryDetailsPage implements OnInit {
  scheduleDetails: any = {};
  data: any;
  isRecord = false;
  loanId: any;
  commonurl:any='';
  constructor(
    private inAppBrowser: InAppBrowser,
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private appService: AppService,
    public modalController: ModalController,
    private base64ToGallery: Base64ToGallery,
    private fileOpener: FileOpener,
    private file: File
  ) { }

  ngOnInit() {
    this.paymentScheduleDetails();
  }

  paymentScheduleDetails() {
    this.route.queryParams.subscribe(params => {
      if (params.loan_id) {
        this.loanId = params;
        this.appService.showLoading();
        this.appService.appPost(params, 'getRePaymentScheduleDetails')
          .subscribe(response => {
            console.log(response)
            if (response.body.status) {
              this.scheduleDetails = response.body;
              console.log(this.scheduleDetails)
              setTimeout(() => {
                this.appService.hideLoading();
              }, 500);
            }
          });
      }
    });
  }

  getReceipt(id: number,loanfrom:any,istallment_count:any) {
    console.log('laonfrom',loanfrom);
    this.appService.showLoading();
    this.appService.appPost({ payment_id: id,loanfrom:loanfrom,loan_id:this.loanId.loan_id,istallment_count:istallment_count }, 'downloadPDF')
      .subscribe(response => {
        if (response.body.status) {
          this.data = response.body.data;
          this.commonurl=response.body.pdf_server_url;
          const browser = this.inAppBrowser.create(this.commonurl, '_system');
          this.appService.hideLoading();
          // setTimeout(() => {
          //   document.getElementById('hiddenclickbtn').click();
          //   this.appService.hideLoading();
          // }, 500);
        }
      });
  }
  requestSOA(loanfrom:any) {
    this.appService.showLoading();
    this.appService.appPost({ ...this.loanId,loanfrom:loanfrom }, 'downloadBulkPDF')
      .subscribe(response => {
        console.log(response)
        if (response.body.status) {
          this.cd.detectChanges();
          this.commonurl=response.body.pdf_server_url;
       
          this.isRecord = false;
          this.data = response.body.data;
          const downloadPDF: any = response.body.url;
          console.log(response.body.pdf_server_url);
          const browser = this.inAppBrowser.create(this.commonurl, '_system');
          this.appService.hideLoading();
          // setTimeout(() => {
          //   document.getElementById('hiddenclickbtn').click();
          // //  window.open(response.body.pdf_server_url);
          //  // window.open(response.body.pdf_server_url, '_self', 'location=yes');
          //   this.appService.hideLoading();
          // }, 500);
        } else {
          this.isRecord = true;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        }
      });
  }
  async requestSOA1() {
    const modal = await this.modalController.create({
      component: RequestSOAComponent,
      cssClass: 'custom_modal',
      componentProps: {
        loan: this.loanId
      }
    });
    return await modal.present();
  }
}
