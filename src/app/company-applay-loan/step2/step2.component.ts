import { Component, OnInit } from '@angular/core';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss'],
})
export class Step2Component implements OnInit {
  formFieldsList = [];
  start = 18;
  end = 27;
  user: any = {};
  nric: number;
  radioList = [{ name: 'Yes', value: 'yes' }, { name: 'No', value: 'no' }];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertController: AlertController
  ) {
    this.getFormField();
  }

  ngOnInit() {
   }

  getFormField() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        this.formFieldsList = data.data;
        const radioData = this.formFieldsList.filter(x => x.slug === 'outstanding_loan')[0];
        if (radioData.modal === '') {
          radioData.modal = 'yes';
        }
        this.nric = data.nric;
      }
    });
  }
  next = () => {
    this.user = { data: this.formFieldsList, nric: this.nric };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigate(['/company-applay-loan/step3'], navigationExtras);
  }

  saveDarft = () => {
    const saveData = { type: 1, data: this.formFieldsList, nric: this.nric };
    localStorage.setItem('a_c_draft_company', JSON.stringify(saveData));
    this.confirmDraftDialog();
  }

  checkMoneylender(event) {
    const data = this.formFieldsList.filter(x => x.slug === 'name_of_license_moneylender')[0];
    const data1 = this.formFieldsList.filter(x => x.slug === 'loan_amount')[0];
    const data2 = this.formFieldsList.filter(x => x.slug === 'payment_frequency')[0];
    if (event.detail.value === 'no') {
      data.isDisabled = 'moneylender';
      data.is_required = 'false';
      data.modal = '';

      data1.isDisabled = 'moneylender';
      data1.is_required = 'false';
      data1.modal = '';

      data2.isDisabled = 'moneylender';
      data2.is_required = 'false';
      data2.modal = '';
    } else {
      data.isDisabled = '';
      data.is_required = 'true';
      data.modal = '';

      data1.isDisabled = '';
      data1.is_required = 'true';
      data1.modal = '';

      data2.isDisabled = '';
      data2.is_required = 'true';
      data2.modal = '';
    }
  }

  async confirmDraftDialog() {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: 'Save as draft successfully',
      cssClass: ['alert_box', 'text-green'],
      buttons: ['OK']
    });
    await alert.present();
  }
}
