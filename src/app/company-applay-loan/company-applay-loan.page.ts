import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/_service/app.service';

@Component({
  selector: 'app-company-applay-loan',
  templateUrl: './company-applay-loan.page.html',
  styleUrls: ['./company-applay-loan.page.scss'],
})
export class CompanyApplayLoanPage implements OnInit {
  formFieldsList = [];
  start = 0;
  end = 9;
  user: any = {};
  isMailExist: any;
  isLogin = false;
  nric: number;
  isValidMail = '';
  @ViewChild('f', { static: false }) loanForm: any;
  constructor(
    private router: Router,
    private appService: AppService,
    private route: ActivatedRoute
  ) {
    this.isLogin = this.appService.isLogin();
    console.log(this.checkMail('dfjd@gmail.com'))
  }

  ngOnInit() {
  
    const localData = JSON.parse(localStorage.getItem('a_c_draft_company'));
    if (localData !== null) {
      this.formFieldsList = localData.data;
    } else {
      this.getFields();
    }
    this.route.queryParams.subscribe(params => {
      this.nric = params.nric;
    })
  }

  getFields = () => {
    this.appService.showLoading();
    if (this.isLogin) {
      this.appService.appPost({ registration_type: 4 }, 'getFormFields')
        .subscribe(response => {
          if (response.body.status) {
            this.formFieldsList = response.body.data;
            setTimeout(() => {
              this.appService.hideLoading();
            }, 500);
          }
        });
    } else {
      this.appService.appLogin({ registration_type: 4 }, 'getFormFields')
        .subscribe(response => {
          if (response.body.status) {
            this.formFieldsList = response.body.data;
            console.log(this.formFieldsList)
            setTimeout(() => {
              this.appService.hideLoading();
            }, 500);
          }
        });
    }
  }

  checkMail = (mailId: any) => {

    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailId);
    if (checkValid) {
      this.isValidMail = '';
      if (this.isLogin) {
        return;
      } else {
        this.appService.appLogin({ email: mailId }, 'checkEmail')
          .subscribe(response => {
            if (response.body.status) {
              return;
            } else {
              this.isMailExist = response.body.msg;
            }
          });
      }
    } else {
      this.isValidMail = 'invalid';
    }

  }



  next = () => {
    console.log(this.loanForm.valid, this.loanForm.FormGroup)
    if (this.loanForm.valid) {
      this.user = { data: this.formFieldsList, nric: this.nric };
      const navigationExtras: NavigationExtras = {
        state: {
          user: this.user
        }
      };
      this.router.navigate(['/company-applay-loan/step1'], navigationExtras);
    }

  }
}
