import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CompanyApplayLoanPage } from './company-applay-loan.page';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
const routes: Routes = [
  {
    path: '',
    component: CompanyApplayLoanPage
  }, {
    path: 'step1',
    component: Step1Component
  }, {
    path: 'step2',
    component: Step2Component
  }, {
    path: 'step3',
    component: Step3Component
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ionic4DatepickerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CompanyApplayLoanPage,
    Step1Component,
    Step2Component,
    Step3Component
  ]
})
export class CompanyApplayLoanPageModule { }
