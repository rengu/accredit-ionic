import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from'@logisticinfotech/ionic4-datepicker';
@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss'],
})
export class Step1Component implements OnInit {
  formFieldsList = [];
  start = 9;
  end = 18;
  user: any = {};
  nric: number;
  datePickerObj:any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertController: AlertController,
    public modalController: ModalController
  ) {
    this.getFormField();
  }

  ngOnInit() {
    var today1 = new Date();
    this.datePickerObj = {
      dateFormat: 'YYYY-MM-DD',
      showTodayButton:false,
      fromDate: new Date('1980-03-14'), // default null
      toDate: today1, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    };
   }
   async openDatePicker(title) {
    const dobdata = this.formFieldsList.filter(x => x.title === title)[0];
    console.log('dobdata',dobdata);
    var item =item;
    var selectedDate=new  Date();
    const datePickerModal = await this.modalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'objConfig': this.datePickerObj, 
         'selectedDate':selectedDate,
         'titleLabel': 'Select a Date',
         'dateFormat': 'YYYY-MM-DD'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date != "Invalid date")
        {
          dobdata.modal=data.data.date;
          console.log(this.formFieldsList);
        }
        
      });
  }
  getFormField() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        this.formFieldsList = data.data;
        this.nric = data.nric;
      }
    });
  }
  next = () => {
    this.user = { data: this.formFieldsList, nric: this.nric };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigate(['/company-applay-loan/step2'], navigationExtras);
  }

  saveDarft = () => {
    const saveData = { type: 1, data: this.formFieldsList, nric: this.nric };
    localStorage.setItem('a_c_draft_company', JSON.stringify(saveData));
    this.confirmDraftDialog();
  }
  async confirmDraftDialog() {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: 'Save as draft successfully',
      cssClass: ['alert_box', 'text-green'],
      buttons: ['OK']
    });
    await alert.present();
  }
}
