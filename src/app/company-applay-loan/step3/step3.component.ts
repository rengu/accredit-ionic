import { Component, OnInit } from '@angular/core';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/_service/app.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss'],
})
export class Step3Component implements OnInit {
  formFieldsList = [];
  start = 27;
  end = 40;
  proofOfIncome: any = {};
  otherDocument: any = {};
  user: any = {};
  isLogin = false;
  nric: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public alertController: AlertController,
    private appService: AppService
  ) {
    this.isLogin = this.appService.isLogin();
    this.getFormField();
  }

  ngOnInit() { }

  getFormField() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        this.formFieldsList = data.data;
        this.user.nric = data.nric;
      }
    });
  }


  onFileChanged(event: any, slug: any) {
    console.log(event.target.files)
    if (slug === 'proof_of_income') {
      this.proofOfIncome = event.target.files[0];
    } else {
      this.otherDocument = event.target.files[0];
    }
  }

  applyLoan = () => {
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
    if(!this.user)
      this.user = [];
    this.appService.showLoading();
    this.formFieldsList.forEach((item) => {
      this.user[item.slug] = item.modal;
    });
    const date = this.user.financial_year_end_as_at.split('T');
    this.user.financial_year_end_as_at = date[0];
    if (this.isLogin) {
   
      const token = JSON.parse(localStorage.getItem('a_c_uid'));
     
      this.user.is_token = 1;
      this.user.token = token.token;
    } else {
      this.user.is_token = 0;
    }
    const formData = new FormData();
    formData.append('Body', JSON.stringify(this.user));
    formData.append('proof_of_income ', this.proofOfIncome);
    formData.append('other_document  ', this.otherDocument);

    this.appService.postUrlEncoded(formData, 'applyLoanCompany')
      .subscribe(response => {
        if (response.status) {
          this.appService.hideLoading();
          localStorage.removeItem('a_c_draft_company');
          this.confirmDialog(response.msg);
        } else {
          this.appService.hideLoading();
          this.presentAlert(response.msg);
        }
      });
  }

  async presentAlert(sms: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: sms,
      cssClass: ['alert_box', 'text-red'],
      buttons: ['OK']
    });
    await alert.present();
  }

  async confirmDialog(sms: any) {
    const alert = await this.alertController.create({
      header: 'Congratulation!',
      message: sms,
      cssClass: ['alert_box', 'text-green'],
      buttons: [{
        text: 'OK',
        handler: () => {
          if (this.isLogin) {
            this.router.navigate(['/dashboard']);
          } else {
            this.router.navigate(['/home']);
          }
        }
      }
      ]
    });
    await alert.present();
  }

  saveDarft = () => {
    const saveData = { type: 2, data: this.formFieldsList };
    localStorage.setItem('a_c_draft_company', JSON.stringify(saveData));
    this.confirmDraftDialog();
  }

  async confirmDraftDialog() {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: 'Save as draft successfully',
      cssClass: ['alert_box', 'text-green'],
      buttons: ['OK']
    });
    await alert.present();
  }
}
