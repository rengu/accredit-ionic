import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  token;
  isLogin = false;
  constructor(private appService: AppService) {
    this.token = localStorage.getItem('stoken');
    this.isLogin = this.appService.isLogin();
  }

  ngOnInit() {

  }
}
