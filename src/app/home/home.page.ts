import { Component } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AppService } from 'src/app/_service/app.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  token: any;
  isLogin = false;
  constructor( public menuCtrl: MenuController, public navCtrl: NavController, private appService: AppService) {
    this.isLogin = this.appService.isLogin();
    console.log('home page called.');
    // this.token = localStorage.getItem('stoken');
    // localStorage.removeItem('a_c_uid');
    // var x=document.getElementById('sideDrwer');
    // x.style.display = "none"; 
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    console.log(this.isLogin);
    if (this.isLogin) {
      this.navCtrl.navigateRoot('/dashboard');
    }
  }
}
