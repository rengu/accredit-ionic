import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';

@Component({
  selector: 'app-payment-schedule',
  templateUrl: './payment-schedule.page.html',
  styleUrls: ['./payment-schedule.page.scss'],
})
export class PaymentSchedulePage implements OnInit {
  scheduleList: any[] = [];
  noData: any;
  constructor(
    private appService: AppService
    ) { }

  ngOnInit() {
    this.getScheduleList();
  }

  getScheduleList() {
    this.appService.showLoading();
    this.appService.appPost({}, 'getPaymentScheduleList')
      .subscribe(response => {
        if (response.body.status) {
          this.scheduleList = response.body.data;
          console.log(this.scheduleList);
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        } else {
          this.noData = response.body.msg;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        }
      });
  }
}
