import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/_service/app.service';

@Component({
  selector: 'app-payment-schedule-list',
  templateUrl: './payment-schedule-list.component.html',
  styleUrls: ['./payment-schedule-list.component.scss'],
})
export class PaymentScheduleListComponent implements OnInit {
  scheduleDetails: any = {};
  noData: any;
  constructor(
    private route: ActivatedRoute,
    private appService: AppService
  ) { }

  ngOnInit() {
    this.paymentScheduleDetails();
  }

  paymentScheduleDetails() {
    this.route.queryParams.subscribe(params => {
      if (params.loan_id) {
        this.appService.showLoading();
        this.appService.appPost(params, 'getPaymentScheduleDetails')
          .subscribe(response => {
            console.log(response)
            if (response.body.status) {
              this.scheduleDetails = response.body;
              setTimeout(() => {
                this.appService.hideLoading();
              }, 300);
            } else {
              this.noData = response.body.msg;
              setTimeout(() => {
                this.appService.hideLoading();
              }, 500);
            }
          });
      }
    });
  }
}
