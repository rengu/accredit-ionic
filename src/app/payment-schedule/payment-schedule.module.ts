import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaymentSchedulePage } from './payment-schedule.page';
import { PaymentScheduleListComponent } from './payment-schedule-list/payment-schedule-list.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentSchedulePage
  }, {
    path: 'details',
    component: PaymentScheduleListComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PaymentSchedulePage,
    PaymentScheduleListComponent
  ]
})
export class PaymentSchedulePageModule {}
