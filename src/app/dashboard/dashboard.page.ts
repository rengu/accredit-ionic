import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss', '../home/home.page.scss'],
})
export class DashboardPage implements OnInit {
  isLogin: any;
  constructor(public menuCtrl: MenuController ) {
  }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

}
