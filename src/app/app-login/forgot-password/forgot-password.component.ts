import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../_service/app.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private router: Router,
    public alertController: AlertController
  ) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  doLogin = () => {
    this.appService.showLoading();
    this.appService.appLogin(this.loginForm.value, 'forgotpassword')
      .subscribe(response => {
        if (response.body.status) {   
          this.appService.hideLoading();            
          this.confirmDialog(response.body.msg); 
        } else {
          this.appService.hideLoading();
          this.presentAlert(response.body.msg)    
        }
      });
  }

 async confirmDialog(sms: any) {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: sms,
      cssClass: ['alert_box', 'text-green'],
      buttons: [{
        text: 'OK',
        handler: () => {
          this.router.navigate(['/app-login']);
        }
      }
      ]
    });
    await alert.present();
  }

  async presentAlert(sms: any) {
    const alert = await this.alertController.create({
      header: 'Alert!',
      message: sms,
      cssClass: ['alert_box', 'text-red'],
      buttons: ['OK']
    });
    await alert.present();
  }

}
