import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../_service/app.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {InAppBrowser, InAppBrowserOptions,InAppBrowserEvent} from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-app-login',
  templateUrl: './app-login.page.html',
  styleUrls: ['./app-login.page.scss'],
})
export class AppLoginPage implements OnInit {
  loginForm: FormGroup;
  isError = false;
  otp: any;
  constructor(
    private inAppBrowser: InAppBrowser,
    public platform: Platform,
    private formBuilder: FormBuilder,
    private appService: AppService,
    private router: Router,
    public alertController: AlertController
  ) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  doLogin = () => {
    this.appService.showLoading();
    this.appService.appLogin(this.loginForm.value, 'login')
      .subscribe(response => {
        if (response.body.status) {
          
          console.log(JSON.stringify(response.body.data));
          localStorage.setItem('tempdata', JSON.stringify(response.body.data));

         // localStorage.setItem('a_c_uid', JSON.stringify(response.body.data));
          if (response.body.data.is_loginfirst === '1') {
            this.router.navigate(['/signup/register-step'], { queryParams: { type: response.body.data.registration_type,  is_loginfirst: '1'} });
          } else {
            this.router.navigate(['/app-login/verification'], {queryParams: {mobile: response.body.data.phone}});
           // this.router.navigate(['/dashboard']);
          }
          this.appService.hideLoading();
        } else {
          this.isError = true;
          this.appService.hideLoading();
          this.presentAlert(response.body.msg)
        }
      },(error) => {
          this.isError = true;
          this.appService.hideLoading();
          this.presentAlert("Unknown Error Occured.")
      });
  }

  async presentAlert(sms: any) {
    const alert = await this.alertController.create({
      header: 'Alert!',
      message: sms,
      cssClass: ['alert_box', 'text-red'],
      buttons: ['OK']
    });
    await alert.present();
  }
  opendocument(url:any)
  {
    var tgt = this.platform.is('desktop') ? '_system' : '_blank';
    const browser = this.inAppBrowser.create(url, tgt);
    console.log(browser,'browser')
  }
}
