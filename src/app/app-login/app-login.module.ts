import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component'
import { IonicModule } from '@ionic/angular'; 

import { AppLoginPage } from './app-login.page';
import { AppService } from '../_service/app.service';
import { CodeVerificationPage } from './code-verification/code-verification.page';

const routes: Routes = [
  {
    path: '',
    component: AppLoginPage
  }, {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  }, {
    path: 'verification',
    component: CodeVerificationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  declarations: [AppLoginPage, ForgotPasswordComponent, CodeVerificationPage],
  providers: [AppService]
})
export class AppLoginPageModule {}
