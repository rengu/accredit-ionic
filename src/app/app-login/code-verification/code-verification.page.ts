import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../_service/app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-code-verification',
  templateUrl: './code-verification.page.html',
  styleUrls: ['./code-verification.page.scss'],
})
export class CodeVerificationPage implements OnInit {
  otpVerificatin: FormGroup;
  mobileNumber: any = {};
  @ViewChild('otp1', { static: false }) input1;
  constructor(
    private appService: AppService,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.mobileNumber = params;
    })
    this.otpVerificatin = this.formBuilder.group({
      otpVal1: ['', Validators.required],
      otpVal2: ['', Validators.required],
      otpVal3: ['', Validators.required],
      otpVal4: ['', Validators.required]
    });
    setTimeout(() => {
      this.input1.setFocus();
    }, 500);
  }

  otpAction(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus();
    } else if (next && event.target.value.length > 0) {
      next.setFocus();
    } else {
      return 0;
    }
  }

  doLogin = () => {
    const sendtoken = localStorage.getItem('ac_nf_token');
    if(sendtoken)
    {
      var token = sendtoken;
    }
    else{
      var token = '';
    }
    const motp = this.otpVerificatin.value.otpVal1 + this.otpVerificatin.value.otpVal2 + this.otpVerificatin.value.otpVal3 + this.otpVerificatin.value.otpVal4
    this.appService.showLoading();
    console.log('motp',motp)
    console.log('token',token)

    this.appService.appPost({ otp: motp, firebase_token: token }, 'verifyOtp')
      .subscribe(response => {
        if (response.body.status) {
          localStorage.setItem('a_c_uid', localStorage.getItem('tempdata'));
          localStorage.removeItem('tempdata');
          this.router.navigate(['/dashboard']);
          this.appService.hideLoading();
          this.presentToast(response.body.msg);
        } else {
          this.appService.hideLoading();
          this.presentToast(response.body.msg);
        }
      });
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      animated: true
    });
    toast.present();
  }

  resendOtp() {
    this.appService.appPost({}, 'resendOtp')
      .subscribe(response => {
        if (response.body.status) {
          this.presentToast(response.body.msg);
        } else {
          this.presentToast(response.body.msg);
        }
      });
  }
}
