import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from'@logisticinfotech/ionic4-datepicker';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss'],
})
export class Step3Component implements OnInit {
  formFieldsList = [];
  start = 26;
  end = 36;
  user: any = {};
  nric: number;
  uid:any='';
  datePickerObj:any;
  today2:any='';
  isValidNokphone:any=true;
  radioList = [{ name: 'Yes', value: 'yes'}, { name: 'No', value: 'no'}]
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertController: AlertController,
    public modalController: ModalController
  ) {
    this.getFormField();
  }

  ngOnInit() {
    this.today2 = new Date().toJSON().slice(0,10).replace(/-/g,'-');  
    var today1 = new Date();
    var year = today1.getFullYear();
    var month = today1.getMonth();
    var day = today1.getDate();
    var todayaddoneyear = new Date(year + 1, month, day);
  
     var today1 = new Date();
    this.datePickerObj = {
      //dateFormat: 'YYYY-MM-DD',
      dateFormat: 'DD-MM-YYYY',
      showTodayButton:false,
      fromDate: new Date('1980-03-14'), // default null
      //fromDate: new Date('14-03-1980'),
      toDate: todayaddoneyear, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    }; 
  }
  checknumber(numberdata:any,slug:any){
    console.log('slug',slug)
    if(slug == 'next_of_kin_contact_number')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidNokphone = true;
      } else {
        this.isValidNokphone = false;
      }
    }
  }
  
  getFormField() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        this.formFieldsList = data.data;
        console.log(this.formFieldsList);
        const radioData = this.formFieldsList.filter(x => x.slug === 'outstanding_loan')[0];
        console.log(radioData);
        if(radioData)
        {
          if (radioData.modal === '') {
            radioData.modal = 'yes';
          }
        }
        this.nric = data.nric;
        this.uid = data.uid;
      }
      console.log('this.uid', this.uid);
    });
  }

  saveDarft = () => {
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
    const saveData = { type: 2, data: this.formFieldsList, nric: this.nric };
    if(this.user){

      localStorage.setItem('a_c_draft_'+this.user.user_id, JSON.stringify(saveData));
    }else{
      localStorage.setItem('a_c_draft', JSON.stringify(saveData));
    }
    this.confirmDraftDialog();
  }

  checkMoneylender(event) {
    const data = this.formFieldsList.filter(x => x.slug === 'name_of_license_moneylender')[0];
    const data1 = this.formFieldsList.filter(x => x.slug === 'loan_amount')[0];
    const data2 = this.formFieldsList.filter(x => x.slug === 'payment_frequency')[0];

    if (event.detail.value === 'no') {
      data.isDisabled = 'moneylender';
      data.is_required = 'false';
      data.modal = '';

      data1.isDisabled = 'moneylender';
      data1.is_required = 'false';
      data1.modal = '';

      data2.isDisabled = 'moneylender';
      data2.is_required = 'false';
      data2.modal = '';
    } else {
      data.isDisabled = '';
      data.is_required = 'true';
      data.modal = '';

      data1.isDisabled = '';
      data1.is_required = 'true';
      data1.modal = '';

      data2.isDisabled = '';
      data2.is_required = 'true';
      data2.modal = '';
    }
  }

  async confirmDraftDialog() {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: 'Save as draft successfully',
      cssClass: ['alert_box', 'text-green'],
      buttons: ['OK']
    });
    await alert.present();
  }

  next = () => {
    this.user = { data: this.formFieldsList, nric: this.nric, uid: this.uid };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      },
      replaceUrl: true
    };
    this.router.navigate(['/individual-applay-loan/step4'], navigationExtras);
  }
  async openDatePicker(title) {
    const dobdata = this.formFieldsList.filter(x => x.title === title)[0];
   
    var item =item;
    var selectedDate=new  Date();
    const datePickerModal = await this.modalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'objConfig': this.datePickerObj, 
         'selectedDate':selectedDate,
         'titleLabel': 'Select a Date',
         'dateFormat': 'YYYY-MM-DD'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date != "Invalid date")
        {
          dobdata.modal=data.data.date;
          console.log(this.formFieldsList);
        }
        
      });
  }

}
