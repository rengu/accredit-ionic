import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from'@logisticinfotech/ionic4-datepicker';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss'],
})
export class Step1Component implements OnInit {
  formFieldsList = [];
  start = 10;
  end = 16;
  user: any = {};
  nric: number;
  uid:any='';
  datePickerObj:any;
  isValidpostal:any=true;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertController: AlertController,
    public modalController: ModalController
  ) {
    this.getFormField();
  }

  ngOnInit() {
    var today1 = new Date();
    console.log('today1',today1);
    this.datePickerObj = {
      //dateFormat: 'YYYY-MM-DD',
      dateFormat: 'DD-MM-YYYY',
      showTodayButton:false,
      //fromDate: new Date('1980-03-14'), // default null
      fromDate: new Date('14-03-1980'), // default null
      toDate: today1, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    }; 
   }

  getFormField() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        this.formFieldsList = data.data;
        this.nric = data.nric;
        this.uid = data.uid;
      }
      console.log('this.uid', this.uid);
    });
  }
  checknumber(numberdata:any,title:any){
    console.log('slug',title)
    if(title == 'Postal Code')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidpostal = true;
      } else {
        this.isValidpostal = false;
      }
    }
  }
  next = () => {
    this.user = { data: this.formFieldsList, nric: this.nric, uid: this.uid };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      },
      replaceUrl: true
    };
    
    this.router.navigate(['/individual-applay-loan/step2'], navigationExtras);
  }

  saveDarft = () => {
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
    const saveData = { type: 1, data: this.formFieldsList, nric: this.nric };
    if(this.user){

      localStorage.setItem('a_c_draft_'+this.user.user_id, JSON.stringify(saveData));
    }else{
      localStorage.setItem('a_c_draft', JSON.stringify(saveData));
    }
    this.confirmDraftDialog();
  }
  async confirmDraftDialog() {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: 'Save as draft successfully',
      cssClass: ['alert_box', 'text-green'],
      buttons: ['OK']
    });
    await alert.present();
  }

  addMore = (type: number, data: any) => {
    if (data.modal) {
      const num = parseInt(data.modal, 10);
      if (type === 1) {
        data.modal = num + 1;
      } else if (num >= 1) {
        data.modal = num - 1;
      }
    } else {
      data.modal = 1;
    }
  }
  async openDatePicker(title) {
    const dobdata = this.formFieldsList.filter(x => x.title === title)[0];
   
    var item =item;
    var selectedDate=new  Date();
    const datePickerModal = await this.modalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'objConfig': this.datePickerObj, 
         'selectedDate':selectedDate,
         'titleLabel': 'Select a Date',
         //'dateFormat': 'YYYY-MM-DD'
         'dateFormat': 'DD-MM-YYYY'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date != "Invalid date")
        {
          dobdata.modal=data.data.date;
          console.log(this.formFieldsList);
        }
        
      });
  }
}
