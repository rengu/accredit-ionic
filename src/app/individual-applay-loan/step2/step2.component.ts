import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AppService } from 'src/app/_service/app.service';
import { Ionic4DatepickerModalComponent } from'@logisticinfotech/ionic4-datepicker';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss'],
})
export class Step2Component implements OnInit {
  formFieldsList = [];
  start = 16;
  end = 26;
  proofOfIncome: any = {};
  otherDocument: any = {};
  user: any = {};
  nric: number;
  uid:any='';
  isLogin = false;
  datePickerObj:any;
  today2:any='';
  isValidofficenumber:any=true;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertController: AlertController,
    private appService: AppService,
    public modalController: ModalController
  ) {
    this.getFormField();
  }

  ngOnInit() {   
    this.today2 = new Date(new Date().setFullYear(new Date().getFullYear() + 2)).toJSON().slice(0,10).replace(/-/g,'-');
    var today1 = new Date();
    this.datePickerObj = {
      //dateFormat: 'YYYY-MM-DD',
      dateFormat: 'DD-MM-YYYY',
      showTodayButton:false,
      fromDate: new Date('1980-03-14'), // default null
     // fromDate: new Date('14-03-1980'), // default null
      toDate: today1, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    };
  }
  checknumber(numberdata:any,slug:any){
    console.log('slug',slug)
    if(slug == 'office_number')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidofficenumber = true;
      } else {
        this.isValidofficenumber = false;
      }
    }
  }
  
  getFormField() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        this.formFieldsList = data.data;
        this.nric = data.nric;
        this.uid = data.uid;
      }
      console.log('this.uid', this.uid);
    });
  }

  saveDarft = () => {
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
    const saveData = { type: 2, data: this.formFieldsList, nric: this.nric};
    if(this.user){

      localStorage.setItem('a_c_draft_'+this.user.user_id, JSON.stringify(saveData));
    }else{
      localStorage.setItem('a_c_draft', JSON.stringify(saveData));
    }
    this.confirmDraftDialog();
  }
  async confirmDraftDialog() {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: 'Save as draft successfully',
      cssClass: ['alert_box', 'text-green'],
      buttons: ['OK']
    });
    await alert.present();
  }

  onFileChanged(event: any, slug: any) {
    console.log(event.target.files)
    if (slug === 'proof_of_income') {
      this.proofOfIncome = event.target.files[0];
    } else {
      this.otherDocument = event.target.files[0];
    }
  }
  
  next = () => {
    this.user = { data: this.formFieldsList, nric: this.nric, uid: this.uid };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      },
      replaceUrl: true
    };
    if(this.uid) {
      navigationExtras['queryParams'] = { 'uid': this.uid };
    }
    this.router.navigate(['/individual-applay-loan/step3'], navigationExtras);
  }
  
  applyLoan = () => {
    localStorage.removeItem('myinfo_callback_key');
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
    if(!this.user)
      this.user = {};
    this.appService.showLoading();
    this.formFieldsList.forEach((item) => {
      this.user[item.slug] = item.modal;
    });
    console.log('sss',this.user);
   /*  const date = this.user.pay_date_of_salary.split('T');
    this.user.pay_date_of_salary = date[0]; */
    if(this.user.start_date_of_employment)
    {
     // this.user.start_date_of_employment=this.datepipe.transform(this.user.start_date_of_employment, 'YYYY-MM-DD');
      const date1 = this.user.start_date_of_employment.split('T');
      this.user.start_date_of_employment = date1[0];  
    }

    this.user.pay_date_of_salary= this.user.pay_date_of_salary;
    if (this.isLogin) {
      const token = localStorage.getItem('a_c_uid');
      this.user.is_token = 1;
      this.user.token = token;
    } else {
      this.user.is_token = 0;
    }
    const formData = new FormData();
    formData.append('Body', JSON.stringify(this.user));
    formData.append('proof_of_income ', this.proofOfIncome);
    formData.append('other_document  ', this.otherDocument);
    formData.append('uid', this.uid);
    formData.append('nric', this.nric+"");
    console.log('ttt',this.user);
    console.log(formData);
    this.appService.postUrlEncoded(formData, 'applyLoanIndividual')
      .subscribe(response => {
        if (response.status) {
          this.appService.hideLoading();
          //localStorage.removeItem('a_c_draft');
          if(this.user){
            localStorage.removeItem('a_c_draft_'+this.user.user_id);

          }else{
            localStorage.removeItem('a_c_draft');
          }
          this.confirmDialog(response.msg);
        } else {
          this.appService.hideLoading();
          this.presentAlert(response.msg);
        }
      },(error) => {
          this.appService.hideLoading();
          this.presentAlert("Unknown Error Occured.")
      });
  }

  async presentAlert(sms: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: sms,
      cssClass: ['alert_box', 'text-red'],
      buttons: ['OK']
    });
    await alert.present();
  }

  async confirmDialog(sms: any) {
    const alert = await this.alertController.create({
      header: 'Congratulation!',
      message: sms,
      cssClass: ['alert_box', 'text-green'],
      buttons: [{
        text: 'OK',
        handler: () => {
          if (this.isLogin) {
            this.router.navigate(['/dashboard']);
          } else {
            this.router.navigate(['/home']);
          }
        }
      }
      ]
    });
    await alert.present();
  }

  async openDatePicker(title) {
    const dobdata = this.formFieldsList.filter(x => x.title === title)[0];
    console.log('dobdata',dobdata);
    var item =item;
    var selectedDate=new  Date();
    const datePickerModal = await this.modalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'objConfig': this.datePickerObj, 
         'selectedDate':selectedDate,
         'titleLabel': 'Select a Date',
         'dateFormat': 'YYYY-MM-DD'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date != "Invalid date")
        {
          dobdata.modal=data.data.date;
          console.log(this.formFieldsList);
        }
        
      });
  }

}
