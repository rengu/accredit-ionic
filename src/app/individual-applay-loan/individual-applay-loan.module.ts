import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IndividualApplayLoanPage } from './individual-applay-loan.page';
import { Step2Component } from './step2/step2.component';
import { Step1Component } from './step1/step1.component';
import { Step3Component } from './step3/step3.component';
import { Step4Component } from './step4/step4.component';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';

const routes: Routes = [
  {
    path: '',
    component: IndividualApplayLoanPage
  }, {
    path: 'step1',
    component: Step1Component
  }, {
    path: 'step2',
    component: Step2Component
  }, {
    path: 'step3',
    component: Step3Component
  }, {
    path: 'step4',
    component: Step4Component
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ionic4DatepickerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    IndividualApplayLoanPage,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component
  ]
})
export class IndividualApplayLoanPageModule { }
