import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/_service/app.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-individual-applay-loan',
  templateUrl: './individual-applay-loan.page.html',
  styleUrls: ['./individual-applay-loan.page.scss'],
})
export class IndividualApplayLoanPage implements OnInit {
  formFieldsList = [];
  start = 0;
  end = 10;
  user: any = {};
  isMailExist: any;
  isLogin = false;
  nric: number;
  isValidMail = '';
  isMail = '';
  isphoneexist:any=false;
  isValidphone='';
  uid:any='';
  constructor(
    private router: Router,
    private appService: AppService,
    private route: ActivatedRoute,
    public alertController: AlertController,
  ) {
    this.isLogin = this.appService.isLogin();
    console.log(this.isLogin);
  }
  ionViewWillEnter(){
    this.user = JSON.parse(localStorage.getItem('a_c_uid'));
     this.route.queryParams.subscribe(params => {
      this.nric = params.nric;
       if(params.uid){
         if(localStorage.getItem('myinfo_callback_key') == "1") {
           this.uid=params.uid;
          } else {
           this.presentAlert("Access to this page is expired.");
           return;
         }
       }
       if(this.user){
          var localData = JSON.parse(localStorage.getItem('a_c_draft_'+this.user.user_id));

       }else{
          var localData = JSON.parse(localStorage.getItem('a_c_draft'));
       }
       
       console.log('this.uid', this.uid);
       if (localData !== null) {
         this.formFieldsList = localData.data;
       } else {
          if(this.formFieldsList.length > 0){

          }else{

            this.getFields();
         }
       }
     })
 
  }
   ngOnInit() {
    /*
    this.route.queryParams.subscribe(params => {
      //console.log(this.router.getCurrentNavigation().extras.state);
      console.log(params,'params');
      this.nric = params.nric;
      if(params.uid){
        this.uid=params.uid;
      }
      console.log('this.formFieldsListß',this.formFieldsList);
      const localData = JSON.parse(localStorage.getItem('a_c_draft'));
      if (localData !== null) {
        this.formFieldsList = localData.data;

      } else {
        console.log(this.uid,'this.uid');
        this.getFields();
        //if(this.formFieldsList.length > 0){

        //}else{

         // this.getFields();
       // }
      }
    })
    */ 
  }

  getFields = () => {
    this.appService.showLoading();
    if (this.isLogin) {
      console.log('this.uid',this.uid);
      this.appService.appPost({ registration_type: 3,uid:this.uid}, 'getFormFields')
        .subscribe(response => {
          if (response.body.status) {
            this.formFieldsList = response.body.data;
          } else {
            this.presentAlert("Unknown Error Occured.");
          }
          setTimeout(() => {
           this.appService.hideLoading();
          }, 500);
        }, error => {
          console.log(error);
          this.presentAlert("Unknown Error Occured.");
          setTimeout(() => {
               this.appService.hideLoading();
             }, 500);
        });
    } else {
      this.appService.appLogin({ registration_type: 3,uid:this.uid}, 'getFormFields')
        .subscribe(response => {
          if (response.body.status) {
            this.formFieldsList = response.body.data;
            console.log(this.formFieldsList)
          } else {
            this.presentAlert("Unknown Error Occured.");
          }
          setTimeout(() => {
           this.appService.hideLoading();
          }, 500);
        }, error => {
          console.log(error);
          this.presentAlert("Unknown Error Occured.");
          setTimeout(() => {
           this.appService.hideLoading();
          }, 500);
        });
    }
  }
  checknumber(numberdata:any,slug:any){
    console.log('checkphone');
    console.log('slug',slug)
    if(slug == 'phone')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidphone = '';
      } else {
        this.isValidphone = 'invalid';
      }
    }
  }
  checkMail(mailId: any) {
    console.log('checkemail');
    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailId);
    if (checkValid) {
      this.isValidMail = '';
    } else {
      this.isValidMail = 'invalid';
    }
  }
  /* checkMail = (mailId: any) => {
    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailId);
    if (checkValid) {
      this.isValidMail = '';
      if (this.isLogin) {
        return;
      } else {
        this.appService.appLogin({ email: mailId }, 'checkEmail')
          .subscribe(response => {
            console.log(response)
            if (response.body.status) {
              this.isMailExist = '';
              return;
            } else {
              this.isMailExist = response.body.msg;
            }
          });
      }
    } else {
      this.isValidMail = 'invalid';
    }

  } */
  next = () => {
    const email = this.formFieldsList.filter(x => x.slug === 'email')[0];
    const phone = this.formFieldsList.filter(x => x.slug === 'phone')[0];
    const mailId = email.modal;
    const phoneno = phone.modal;
    this.user = { data: this.formFieldsList, nric: this.nric, uid: this.uid };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      },
      replaceUrl: true
    };

    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailId);
    if (checkValid && this.isValidphone == '') {
      this.isValidMail = '';
      this.isValidphone = '';
      this.router.navigate(['/individual-applay-loan/step1'], navigationExtras);
      /*if (this.isLogin) {
        this.router.navigate(['/individual-applay-loan/step1'], navigationExtras);
      } else {
        this.appService.showLoading();
        this.appService.appLogin({ email: mailId }, 'checkEmail')
          .subscribe(response => {
           
            if (response.body.status) {
              this.isMailExist = '';
              this.appService.appLogin({ phone: phoneno }, 'checkPhone')
                .subscribe(response1 => {
                  this.appService.hideLoading();
                  if (response1.body.status) {
                    this.isphoneexist = false;
                    this.router.navigate(['/individual-applay-loan/step1'], navigationExtras);
                  } else {
                    this.isphoneexist = true;
                    this.presentAlert('This Phone No. is already exist');
                  }
                })
            } else {
              this.appService.hideLoading();
              this.isMailExist = response.body.msg;
            //  this.presentAlert('This Email is already exist');
              this.presentAlert(response.body.msg);
              
            }
          });
      }*/
    } else {
      this.isValidMail = 'invalid';

    }

  }
  async presentAlert(sms: any) {
    const alert = await this.alertController.create({
      header: 'Alert!',
      message: sms,
      cssClass: ['alert_box', 'text-red'],
      buttons: ['OK']
    });
    await alert.present();
  }
}
