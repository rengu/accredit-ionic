import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';
@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.page.html',
  styleUrls: ['./calculator.page.scss'],
})
export class CalculatorPage implements OnInit {
  principal = 2000 ;
  tenure = 2;
  interest = 48;
  interesttoshow=4;
  grandTotal: any;
  isLogin = false;
  constructor(private appService: AppService) {
    this.isLogin = this.appService.isLogin();
     this.compoundInterest([this.principal, this.interest, this.tenure]);
   }

  ngOnInit() {
  }

  amountChange(event) {
    if(event){
      this.compoundInterest([event, this.interest, this.tenure])
    } else {
      this.compoundInterest([0, this.interest, this.tenure])
    }
  }
  changeTenure(a) {
    this.tenure = a.detail.value;
    if(this.principal){
      this.compoundInterest([this.principal, this.interest, this.tenure])
    }
  }

  changeInterest(b) {
    this.interesttoshow = b.detail.value;
    this.interest = (b.detail.value * 12);
    if(this.principal){
      this.compoundInterest([this.principal, this.interest, this.tenure])
    }
  }

  compoundInterest([principal, interest, period]) {
    [principal, interest, period] = [principal, interest, period].map(Number);
    const intr = interest / (12 * 100);
    const emi = principal * intr / (1 - (Math.pow(1 / (1 + intr), period)));
    const totalAmount = emi * period;
    this.grandTotal = totalAmount.toFixed(2);
  }
  
}
