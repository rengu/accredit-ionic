import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-payment-code',
  templateUrl: './payment-code.page.html',
  styleUrls: ['./payment-code.page.scss'],
})
export class PaymentCodePage implements OnInit {
  qrCode: any = {};
  doc: any;
  constructor(
    private appService: AppService,
    private base64ToGallery: Base64ToGallery,
    private fileOpener: FileOpener,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.appService.showLoading();
    this.appService.appPost({}, 'getFrontQRCode')
      .subscribe(response => {
        if (response.body.status) {
          this.qrCode = response.body.data;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 500);
        }
      });
  }
  downloadDoc(doc) {
    const downloadPDF: any = doc.url;
    this.base64ToGallery.base64ToGallery(downloadPDF, { prefix: '_img', mediaScanner: true }).then(
      res => {
        this.presentToast('Saved image to gallery');
        setTimeout(() => {
          this.fileOpener
            .open(res, 'image/png')
          console.log('Saved image to gallery ', res)
        }, 500);

      },
      err => console.log('Error saving image to gallery ', err)
    );
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      animated: true
    });
    toast.present();
  }
}
