import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelectLoanTypePage } from './select-loan-type.page';
import { AppService } from '../_service/app.service';
const routes: Routes = [
  {
    path: '',
    component: SelectLoanTypePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SelectLoanTypePage],
  providers: [AppService]
})
export class SelectLoanTypePageModule {}
