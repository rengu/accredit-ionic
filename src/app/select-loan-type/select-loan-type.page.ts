import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../_service/app.service';
import { environment } from '../../environments/environment';
import { AlertController, ToastController } from '@ionic/angular';
import {InAppBrowser, InAppBrowserOptions,InAppBrowserEvent} from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-select-loan-type',
  templateUrl: './select-loan-type.page.html',
  styleUrls: ['./select-loan-type.page.scss'],
})
export class SelectLoanTypePage implements OnInit {
  public options: InAppBrowserOptions = {
    location: 'yes',//Or 'no'
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only
    toolbar: 'yes', //iOS only
    enableViewportScale: 'no', //iOS only
    allowInlineMediaPlayback: 'no',//iOS only
    presentationstyle: 'pagesheet',//iOS only
    fullscreen: 'yes',//Windows only
    footer: 'yes'
};
  agree = false;
  type = 'individual';
  isLogin = false;
  user:any;

  constructor(
    private appService: AppService,
    private router: Router,
    private inAppBrowser: InAppBrowser,
    public platform: Platform,
    public alertController: AlertController,
    public toastController: ToastController) {
    this.isLogin = this.appService.isLogin();
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('loanstype'));
    if(!this.user)
      this.user = {};
  }
  selectSignup = (type: any) => {
    this.type = type;
  }

  userRegister = () => {
    
    //this.router.navigate(['/' + this.type + '-applay-loan'],{ queryParams: { uid:'S6005055D'}});
    this.router.navigate(['/' + this.type + '-applay-loan'] );
   
  }

  openTerms() {
    this.appService.showLoading();
    let termsUrl = environment.url + '../Accredit_Mobile_Terms_Conditions.pdf';
    var tgt = this.platform.is('desktop') ? '_self' : '_blank';
    const browserRef = this.inAppBrowser.create(termsUrl, tgt, this.options);
    browserRef.on('loadstart').subscribe((event) => { console.log(event + event.url); });
    browserRef.on('loadstop').subscribe((event) => { this.appService.hideLoading(); });
    browserRef.on('loaderror').subscribe((event) => { this.appService.hideLoading(); });
    browserRef.on('exit').subscribe((event) => { this.appService.hideLoading(); });
  }

  openPrivacyPolicy() {
    this.appService.showLoading();
    let termsUrl = environment.url + '../Accredit_Mobile_Privacy_Policy.pdf';
    var tgt = this.platform.is('desktop') ? '_self' : '_blank';
    const browserRef = this.inAppBrowser.create(termsUrl, tgt, this.options);
    browserRef.on('loadstart').subscribe((event) => { console.log(event + event.url); });
    browserRef.on('loadstop').subscribe((event) => { this.appService.hideLoading(); });
    browserRef.on('loaderror').subscribe((event) => { this.appService.hideLoading(); });
    browserRef.on('exit').subscribe((event) => { this.appService.hideLoading(); });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      animated: true
    });
    toast.present();
  }

  callMyinfo() {
    this.platform.ready().then(() => {
      localStorage.setItem('myinfo_callback_key', "1");
      this.appService.showLoading();
      this.appService.myinfo({}, 'login1').subscribe(response => {
        this.appService.hideLoading();
        if (response.body.status) {
          console.log('gotomyinfo', response.body.data)
          if (response.body.data) {
            var tgt = this.platform.is('desktop') ? '_self' : '_blank';
            const browser = this.inAppBrowser.create(response.body.data, tgt, this.options);
            browser.on('loadstop').subscribe((event) => {
            //  this.appService.hideLoading();
                    console.log(event);
                    //if (event.url == sucess) {
                      if(event.url.includes(environment.myInfoUrl))
                      {
                        console.log('eventsss111', event);
                        browser.close();
                      
                        var str = event.url;
                        var uid = str.split("=")[1];
                        if(uid != '')
                        {
                          console.log('before redirect',uid)
                          setTimeout(() => {
                            this.router.navigate(['/individual-applay-loan'], {queryParams: {uid: uid}});
                          }, 500);
                        }
                    } else {
                        console.log('event Else', event);
                    }
                }
            );
          }
        }
      }, error => {
        alert(error);
      });
    });
  }
}
