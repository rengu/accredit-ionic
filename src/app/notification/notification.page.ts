import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  showerror = false;
  notificationList: any = { data: []};
  constructor(private appService: AppService) { }

  ngOnInit() {
    this.getList();
  }
  doRefresh(event2: any) {
    this.appService.showLoading();
    this.appService.appPost({}, 'getNotifications')
    .subscribe(response => {
      event2.target.complete();
      this.notificationList = response.body;
      console.log('this.notificationList',this.notificationList)
      if (this.notificationList.data.length > 0) {
        this.showerror = false;
      } else {
        this.showerror = true;
      }
      setTimeout(() => {
        this.appService.hideLoading();
      }, 1000);
    });
  }
  getList() {
    this.appService.showLoading();
    this.appService.appPost({}, 'getNotifications')
      .subscribe(response => {
        this.notificationList = response.body;
        setTimeout(() => {
          this.appService.hideLoading();
        }, 1000);
      });
  }
}
