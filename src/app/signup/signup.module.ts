import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SignupPage } from './signup.page';
import { LoginComponent } from './login/login.component';
import { RegisterStepComponent } from '../signup/register-step/register-step.component';
import { RegisterComponent } from './register/register.component';
import { AppService } from '../_service/app.service';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';

const routes: Routes = [
  {
    path: '',
    component: SignupPage
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register-step',
    component: RegisterStepComponent
  }, {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ionic4DatepickerModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  declarations: [
    SignupPage,
    LoginComponent,
    RegisterStepComponent,
    RegisterComponent
  ],
  providers: [AppService]
})
export class SignupPageModule { }
