import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppService } from '../_service/app.service';
import { environment } from '../../environments/environment';
import {InAppBrowser, InAppBrowserOptions,InAppBrowserEvent} from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { NavController } from '@ionic/angular';
@Component({

  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  user:any;
  public options: InAppBrowserOptions = {
    location: 'yes',//Or 'no'
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only
    toolbar: 'yes', //iOS only
    enableViewportScale: 'no', //iOS only
    allowInlineMediaPlayback: 'no',//iOS only
    presentationstyle: 'pagesheet',//iOS only
    fullscreen: 'yes',//Windows only
    footer: 'yes'
};
  type = 1;
  constructor(private router: Router,
    private inAppBrowser: InAppBrowser,
    public location: Location,
    public platform: Platform,
    public appService: AppService,
    private navCtrl: NavController,) {
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('loanstype'));
  
  }
  selectSignup = (type: any) => {
    this.type = type;
  }

  userRegister = () => {
    //this.router.navigate(['/signup/register-step'], { queryParams: { type: this.type,uid:'S6005051A' } });
    this.router.navigate(['/signup/register-step'], { queryParams: { type: this.type } });
  
  }
  goBack() {
    this.location.back();
  }
  gotomyinfo1() {
    var tgt = this.platform.is('desktop') ? '_system' : '_blank';
    const browser = this.inAppBrowser.create('https://ionicframework.com/', tgt, this.options);
    console.log(browser,'browser')
    browser.on('loadstop').subscribe((event) => {
            console.log(event);
            let sucess = 'https://ionicframework.com/getting-started';
            if (event.url == sucess) {
                console.log('eventsss111', event);
                browser.close();
            } else {
                console.log('event Else', event);
            }
        }
    );
}
callMyinfo1()
{
  setTimeout(() => {
    
    this.router.navigate(['/signup/register-step'], { queryParams: { type: this.type, uid:12 } });
  }, 1000);
}
callMyinfo() {
    this.appService.showLoading();
    this.appService.myinfo({}, 'login1').subscribe(response => {
      this.appService.hideLoading();
      if (response.body.status) {
        
        console.log('gotomyinfo', response.body.data)
        if (response.body.data) {
          var tgt = this.platform.is('desktop') ? '_self' : '_blank';
          const browser = this.inAppBrowser.create(response.body.data, tgt, this.options);
          console.log(browser,'browser')
          browser.on('loadstop').subscribe((event) => {
          //  this.appService.hideLoading();
                  console.log(event);
                  //if (event.url == sucess) {
                    if(event.url.includes(environment.myInfoUrl))
                    {
                      console.log('eventsss111', event);
                      browser.close();
                    
                      var str = event.url;
                      var uid = str.split("=")[1];
                      if(uid != '')
                      {
                        setTimeout(() => {
                          this.router.navigate(['/signup/register-step'], { queryParams: { type: this.type, uid:uid } });
                        }, 500);
                      }
                  } else {
                      console.log('event Else', event);
                  }
              }
          );
        }
      }/*  else {
        setTimeout(() => {
          this.appService.hideLoading();
        }, 500);
      } */
    });
  }
  getMyinfoData(uid:any)
  {
   

/*     this.appService.myinfo({}, 'getmyinfo').subscribe(response => {
      }) */
  }
}
