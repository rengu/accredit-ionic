import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AppService } from 'src/app/_service/app.service';
import { AlertController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from '@logisticinfotech/ionic4-datepicker';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-register-step',
  templateUrl: './register-step.component.html',
  styleUrls: ['./register-step.component.scss'],
})
export class RegisterStepComponent implements OnInit {
  formFieldsList: any[] = [];
  start = 0;
  end = 8;
  user: any = {};
  type: any;
  confirmPassword: any;
  datePickerObj:any;
  isValidPostal:any=true;
  uid:any='';
  today1:any='';
  constructor(
    private router: Router,
    private appService: AppService,
    public alertController: AlertController,
    private route: ActivatedRoute,
    public ModalController:ModalController,
  ) {
  }
  ionViewWillEnter(){
    this.route.queryParams.subscribe(params => {
      console.log('params',params)
      if(params.uid){
        this.uid=params.uid;
      }
      if (params.type) {
        this.type = params.type;
        this.user.registration_type = params.type;
        if (parseInt(params.type, 10) === 1) {
          this.getFields(1, params.is_loginfirst,this.uid);
        } else {
          this.getFields(2, params.is_loginfirst,this.uid);
          this.end = 9;
        }
      }
    });
  }
  ngOnInit() {
    this.today1 = new Date().toJSON().slice(0,10).replace(/-/g,'-');
    //var utc = new Date()
    this.datePickerObj = {
      //dateFormat: 'YYYY-MM-DD',
      dateFormat: 'DD-MM-YYYY',
      showTodayButton:false,
      //fromDate: new Date('1980-03-14'), // default null
      fromDate: new Date('14-03-1980'), // default null
      toDate: this.today1, // default null
      isSundayHighlighted : {
        fontColor: '#000' // Default null
       } // Default {}
    };

  }


  getFields = (type, isloginfirst,uid:any) => {
    this.appService.showLoading();
    if (isloginfirst === '1') {
      this.appService.appPost({ registration_type: type,uid:uid }, 'getFormFields')
        .subscribe(response => {
          if (response.body.status) {
            this.formFieldsList = response.body.data;
            console.log('this.formFieldsList',this.formFieldsList)
            setTimeout(() => {
              this.appService.hideLoading();
            }, 300);
          }
        });
    } else {
      this.appService.appLogin({ registration_type: type,uid:uid }, 'getFormFields')
        .subscribe(response => {
          if (response.body.status) {
            this.formFieldsList = response.body.data;
            console.log('this.formFieldsList',this.formFieldsList)
            setTimeout(() => {
              this.appService.hideLoading();
            }, 300);
          }
        });
    }
  }

  saveUser = () => {
    console.log(this.formFieldsList);
    this.user = { data: this.formFieldsList, registration_type: this.type };
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigate(['/signup/register'], navigationExtras);
  }

  async openDatePicker() {
    console.log('this.formFieldsList',this.formFieldsList)
    const dobdata = this.formFieldsList.filter(x => x.slug === 'dob')[0];

   if(dobdata['modal'] != '')
   {
    var selectedDate =new  Date(dobdata['modal']) ;
   }
   else{
    var selectedDate = new  Date();
   }
    var item =item;

    const datePickerModal = await this.ModalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'objConfig': this.datePickerObj, 
         'selectedDate':selectedDate,
         'titleLabel': 'Select a Date',
         //'dateFormat': 'YYYY-MM-DD'
         'dateFormat': 'DD-MM-YYYY'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date != "Invalid date")
        {
          dobdata.modal=data.data.date;
          console.log(this.formFieldsList);
        }
        
      });
  }
  async Openpickerfordateregistraion() {
    console.log('this.formFieldsList',this.formFieldsList)
    const dobdata = this.formFieldsList.filter(x => x.slug === 'date_of_registration')[0];

   if(dobdata['modal'] != '')
   {
    var selectedDate =new  Date(dobdata['modal']) ;
   }
   else{
    var selectedDate = new  Date();
   }
    var item =item;

    const datePickerModal = await this.ModalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'objConfig': this.datePickerObj, 
         'selectedDate':selectedDate,
         'titleLabel': 'Select a Date',
         //'dateFormat': 'YYYY-MM-DD'
         'dateFormat': 'DD-MM-YYYY'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date != "Invalid date")
        {
          dobdata.modal=data.data.date;
          console.log(this.formFieldsList);
        }
        
      });
  }
  
  checknumber(numberdata:any,slug:any){
    console.log('checkphone');
    console.log('slug',slug)
    if(slug == 'Postal Code')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidPostal = true;
      } else {
        this.isValidPostal = false;
      }
    }
  }
}
