import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/_service/app.service';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Ionic4DatepickerModalComponent } from'@logisticinfotech/ionic4-datepicker';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  formFieldsList: any[] = [];
  start = 8;
  end: any;
  user: any = {};
  isValidPostal:any=true;
  isStepCompleted = false;
  isPasswordMatched = true;
  isShow = false;
  type: any;
  confirmPassword: any;
  isMailIdExist = false;
  emailEdit = false;
  isValidMail = '';
  checkMailresult: any = true;
  isMailExist: any;
  isphoneexist: any;
  isValidphone1='';
  constructor(
    private appService: AppService,
    public alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const data = this.router.getCurrentNavigation().extras.state.user;
        const email = data.data.filter(x => x.slug === 'email')[0];
        if (email.modal) { 
          this.emailEdit = true;
         }
        this.formFieldsList = data.data;
        this.end = data.data.length;
        if (data.registration_type === '2') {
          this.start = 9;
        }
        this.user.registration_type = data.registration_type;
      }
    });
  }

  ngOnInit() {
  }

  passwordCompaire(a, event) {
    this.isShow = true;
    if (a === event) {
      this.isPasswordMatched = false;
    } else {
      this.isPasswordMatched = true;
    }
  }

  checkMailcopy = (mailId: any) => {
    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailId);
    if (checkValid) {
      this.isValidMail = '';
    } else {
      this.isValidMail = 'invalid';
    }

  }

  checkMail(mailId: any) {
    console.log('checkemail');
    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailId);
    if (checkValid) {
      this.isValidMail = '';
    } else {
      this.isValidMail = 'invalid';
    }
  }

  addMore = (type: number, data: any) => {
    if (data.modal) {
      const num = parseInt(data.modal, 10);
      if (type === 1) {
        data.modal = num + 1;
      } else if (num >= 1) {
        data.modal = num - 1;
      }
    } else {
      data.modal = 1;
    }
  }

  registerUser = () => {
    this.appService.showLoading();
    this.formFieldsList.forEach((item) => {
      this.user[item.slug] = item.modal;
    });
    if (this.user.registration_type === '1') {
      const date = this.user.dob.split('T');
      this.user.dob = date[0];
    } else {
      const reg = this.user.date_of_registration.split('T');
      this.user.date_of_registration = reg[0];
    }
    const checkValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(this.user.email);
    if (checkValid) {
      this.isValidMail = '';
      this.appService.appLogin({ email: this.user.email }, 'checkEmail')
        .subscribe(response => {
          console.log(response)
          if (response.body.status) {
            this.isMailIdExist = false;
            this.isMailExist = '';
            this.appService.appLogin({ phone: this.user.phone }, 'checkPhone')
              .subscribe(data => {
                if (data.body.status) {
                  this.isphoneexist = false;
                  this.saveuser();
                }
                else {
                  this.appService.hideLoading();
                  this.isphoneexist = true;
                  this.presentAlert('This Phone No. is already exist');
                }
              })
          } else {
            this.appService.hideLoading();
            this.isMailIdExist = true;
            this.isMailExist = response.body.msg;
            this.presentAlert('This email is already exist');
          }
        });

    } else {
      this.isValidMail = 'invalid';
    }
  }
  saveuser() {
    this.appService.appLogin(this.user, 'registration')
      .subscribe(response => {
        if (response.body.status) {
          this.appService.hideLoading();
          this.isMailIdExist = false;
          this.confirmDialog(response.body.msg);
          setTimeout(() => {
            this.alertController.dismiss();
            this.router.navigate(['/app-login']);
          }, 3000);
        } else {
          this.isMailIdExist = true;
          this.appService.hideLoading();
          this.presentAlert(response.body.msg);
        }
      });
  }
  async presentAlert(sms: any) {
    const alert = await this.alertController.create({
      header: 'Alert!',
      message: sms,
      cssClass: ['alert_box', 'text-red'],
      buttons: ['OK']
    });
    await alert.present();
  }

  async confirmDialog(sms: any) {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: sms,
      cssClass: ['alert_box', 'text-green'],
      buttons: [{
        text: 'OK',
        handler: () => {
          this.appService.hideLoading();
          this.router.navigate(['/app-login']);
        }
      }
      ]
    });
    await alert.present();
  }
  async openDatePicker() {
    const datePickerModal = await this.modalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: { 
         'titleLabel': 'Select a Date',
         'dateFormat': 'YYYY-MM-DD'
      }
    });
    await datePickerModal.present();
    datePickerModal.onDidDismiss()
      .then((data) => {
        if(data.data.date)
        {
          if(data.data.date != "Invalid date")
          {
            console.log(data);
          }
        }
      });
  }

  checknumber(numberdata:any,slug:any){
    console.log('checkphone');
    console.log('slug',slug)
    if(slug == 'phone')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidphone1 = '';
      } else {
        this.isValidphone1 = 'invalid';
      }
    }
    if(slug == 'residential_address_two')
    {
      const checkValid = /^-?\d+\.?\d*$/.test(numberdata);
      if (checkValid) {
        this.isValidPostal = true;
      } else {
        this.isValidPostal = false;
      }
    }
  }
}
