import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {AccordionModule} from 'ngx-accordion';
import { IonicModule } from '@ionic/angular';
import { AppService } from '../_service/app.service';
import { FaqPage } from './faq.page';

const routes: Routes = [
  {
    path: '',
    component: FaqPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccordionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FaqPage],
  providers: [AppService]
})
export class FaqPageModule {}
