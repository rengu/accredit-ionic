import { Component, OnInit } from '@angular/core';
import { AppService } from '../_service/app.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  faqList: any[] = [];
  isLogin = false;
  constructor(private appService: AppService) {
    this.isLogin = this.appService.isLogin();
   }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.appService.showLoading();
    this.appService.appLogin({}, 'getFaq')
      .subscribe(response => {
        if (response.body.status) {
          this.faqList = response.body.data;
          setTimeout(() => {
            this.appService.hideLoading();
          }, 1000);
        }
      });
  }

}
